<?php

namespace Comet;




require_once(__DIR__ . "/../Pluto/EntityService.php");




abstract class EntityService extends \Pluto\EntityService
{
    public function __construct(\Comet\Mapper $mapper)
    {
        parent::__construct($mapper);
    }
}
