<?php

namespace Comet;




require_once(__DIR__ . "/../Api.php");
require_once(__DIR__ . "/Email.php");




abstract class EmailApi
{
    public function __construct()
    {
        parent::__construct();

        $this->messageCodeTexts[self::ERROR_DEFAULT] = "An error occurred with the email gateway. Please try again.";
    }




    abstract public function Send(\Comet\Email $email);
}
