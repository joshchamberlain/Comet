<?php

namespace Comet;




class Email
{
    protected $domain;
    protected $from;
    protected $to;
    protected $cc;
    protected $bcc;
    protected $subject;
    protected $html;
    protected $text;
    protected $attachments;




    public function __construct()
    {
        $this->domain = "";
        $this->from = "";
        $this->to = array();
        $this->cc = array();
        $this->bcc = array();
        $this->subject = "";
        $this->html = "";
        $this->text = "";
        $this->attachments = array();
    }




    public function SetDomain($domain)
    {
        $this->domain = $domain;
    }

    public function Domain()
    {
        return $this->domain;
    }

    public function SetFrom($from)
    {
        $this->from = $from;
    }

    public function From()
    {
        return $this->from;
    }

    public function SetTo(Array $to)
    {
        $this->to = $to;
    }

    public function To()
    {
        return $this->to;
    }

    public function SetCc(Array $cc)
    {
        $this->cc = $cc;
    }

    public function Cc()
    {
        return $this->cc;
    }

    public function SetBcc(Array $bcc)
    {
        $this->bcc = $bcc;
    }

    public function Bcc()
    {
        return $this->bcc;
    }

    public function SetSubject($subject)
    {
        $this->subject = $subject;
    }

    public function Subject()
    {
        return $this->subject;
    }

    public function SetHtml($html)
    {
        $this->html = $html;
    }

    public function Html()
    {
        return $this->html;
    }

    public function SetText($text)
    {
        $this->text = $text;
    }

    public function Text()
    {
        return $this->text;
    }

    public function SetAttachments(Array $attachments)
    {
        $this->attachments = $attachments;
    }

    public function Attachments()
    {
        return $this->attachments;
    }



}
