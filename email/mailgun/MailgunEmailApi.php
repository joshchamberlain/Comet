<?php

namespace Comet;




require_once(__DIR__ . "/../EmailApi.php");




class MailgunEmailApi extends \Comet\EmailApi
{
    protected $apiKey;
    protected $endpoint;




    public function __construct($apiKey, $endpoint = "https://api.mailgun.net/v3")
    {
        $this->apiKey = $apiKey;
        $this->endpoint = $endpoint;
    }




    public function Send(\Comet\Email $email)
    {
        $this->result = 1;

        $postData = $this->BuildPostDataFromEmail($email);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/" . $this->GetEmailDomain($email) . "/messages");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        curl_setopt($ch, CURLOPT_USERPWD, "api:" . $this->apiKey);

        if (sizeof($email->Attachments()))
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }

        else
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        }

        $json = curl_exec($ch);

        return $this->result;
    }




    protected function GetEmailDomain(\Comet\Email $email)
    {
        $atPos = strpos($email->From(), "@") + 1;
        $domain = substr($email->From(), $atPos, strlen($email->From()) - $atPos);
        return $domain;
    }




    protected function BuildPostDataFromEmail(\Comet\Email $e)
    {
        $p = array();

        if (trim($e->From()))
        {
            $p["from"] = $e->From();
        }
        
        if (sizeof($e->To()))
        {
            $p["to"] = $this->ArrayToCsv($e->To());
        }

        if (sizeof($e->Cc()))
        {
            $p["cc"] = $this->ArrayToCsv($e->Cc());
        }

        if (sizeof($e->Bcc()))
        {
            $p["bcc"] = $this->ArrayToCsv($e->Bcc());
        }

        $attachments = $e->Attachments();
        $numAttachments = sizeof($attachments);

        if ($numAttachments)
        {
            for ($i = 0; $i < $numAttachments; $i++)
            {
                $p["attachment[" . $i . "]"] = new \CURLFile($attachments[$i]);
            }
        }

        $p["subject"] = $e->Subject();
        $p["text"] = $e->Text();
        $p["html"] = $e->Html();

        return $p;
    }





    protected function ArrayToCsv(Array $a)
    {
        $csv = NULL;
        $numValues = sizeof($a);

        if ($numValues > 0)
        {
            $csv = $a[0];
            $numValues = sizeof($a);

            for ($i = 1; $i < $numValues; $i++)
            {
                $csv = $csv . "," . $a[$i];
            }
        }

        return $csv;
    }
}
