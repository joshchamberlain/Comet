<?php

namespace Comet;




require_once(__DIR__ . "/../Pluto/storage/Mapper.php");




abstract class Mapper extends \Pluto\Mapper
{
    public function __construct(\Pluto\Database $database = NULL)
    {
        parent::__construct($database);
    }
}
