<?php

namespace Comet;




require_once(__DIR__ . "/PaymentMethodEntityService.php");




class PaymentMethodEntityServiceFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Comet\PaymentMethodEntityService(new \Comet\PaymentMethodMapper());
    }
}
