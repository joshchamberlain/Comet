<?php

namespace Comet;




require_once(__DIR__ . "/../EntityService.php");
require_once(__DIR__ . "/PaymentMethodMapper.php");
require_once(__DIR__ . "/PaymentMethodFactory.php");




class PaymentMethodEntityService extends \Comet\EntityService
{
    const ERROR_DESCRIPTION = 300;
    const ERROR_TYPE = 301;
    const ERROR_PAYMENT_METHOD_ID = 302;
    const ERROR_CREATED = 303;
    const ERROR_STATUS = 304;




    public function __construct(\Comet\PaymentMethodMapper $paymentMethodMapper)
    {
        parent::__construct($paymentMethodMapper);

        $this->messageCodeTexts[self::ERROR_DESCRIPTION] = "The description provided was invalid.";
        $this->messageCodeTexts[self::ERROR_TYPE] = "The type provided was invalid.";
        $this->messageCodeTexts[self::ERROR_PAYMENT_METHOD_ID] = "The payment method ID provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CREATED] = "The creation time provided was invalid.";
        $this->messageCodeTexts[self::ERROR_STATUS] = "The status provided was invalid.";
    }




    public function Create(\Comet\PaymentMethod $p)
    {
        $this->result = 1;

        if ($this->Validate($p))
        {
            return $this->result;
        }

        if ($this->mapper && $this->mapper->Create($p))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $p->SetPaymentMethodId($p->Id());

        $this->result = 0;

        return $this->result;
    }

    public function Update(\Comet\PaymentMethod $p)
    {
        $this->result = 1;

        $oldId = $p->Id();
        $p->SetId($p->PaymentMethodId());

        if ($this->Validate($p))
        {
            $p->SetId($oldId);
            return $this->result;
        }

        if ($this->mapper && $this->mapper->Update($p))
        {
            $p->SetId($oldId);
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $p->SetId($oldId);

        $this->result = 0;

        return $this->result;
    }

    public function Delete(\Comet\PaymentMethod $p)
    {
        $this->result = 1;

        $oldId = $p->Id();
        $p->SetId($p->PaymentMethodId());

        if ($this->mapper && $this->mapper->Delete($p))
        {
            $p->SetId($oldId);
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $p->SetId($oldId);

        $this->result = 0;

        return $this->result;
    }




    public function Validate(\Comet\PaymentMethod $p = NULL)
    {
        if ($p === NULL)
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT);
            return 1;
        }

        if ($this->validator->ValidateInt($p->Id(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_ID);
        }

        if ($this->validator->ValidateString($p->Code(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CODE);
        }

        if ($this->validator->ValidateString($p->Description(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_DESCRIPTION);
        }

        if ($this->validator->ValidateInt($p->Type(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_TYPE);
        }

        if ($this->validator->ValidateInt($p->Created(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CREATED);
        }

        if ($this->validator->ValidateInt($p->Status(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_STATUS);
        }

        if ($this->messageContainer->NumErrorMessages())
        {
            return 1;
        }

        return 0;
    }

    public function CopyPaymentMethod(\Comet\PaymentMethod &$p1 = NULL, \Comet\PaymentMethod &$p2 = NULL)
    {
        if ($p1 === NULL || $p2 === NULL)
        {
            return;
        }

        $p2->SetCode($p1->Code());
        $p2->SetDescription($p1->Description());
        $p2->SetType($p1->Type());
        $p2->SetCreated($p1->Created());
        $p2->SetStatus($p1->Status());

        return $p2;
    }




    protected function PopulateEntity(&$o)
    {
        if ($o)
        {
        }

        return $o;
    }
}
