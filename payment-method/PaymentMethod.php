<?php

namespace Comet;




require_once(__DIR__ . "/../Entity.php");




class PaymentMethod extends \Comet\Entity
{
    protected $description;
    protected $type;
    protected $paymentMethodId;
    protected $created;
    protected $status;




    const TYPE_NONE = 0;
    const TYPE_DEFAULT = 1;
    const TYPE_STRIPE = 2;
    const TYPE_PAYPAL = 3;
    const TYPE_BITCOIN = 4;




    public function __construct()
    {
        parent::__construct();

        $this->description = "";
        $this->type = 0;
        $this->paymentMethodId = 0;
        $this->created = 0;
        $this->status = 0;
    }




    public function SetDescription($description)
    {
        $this->description = $description;
    }

    public function Description()
    {
        return $this->description;
    }

    public function SetType($type)
    {
        $this->type = $type;
    }

    public function Type()
    {
        return $this->type;
    }

    public function SetPaymentMethodId($paymentMethodId)
    {
        $this->paymentMethodId = $paymentMethodId;
    }

    public function PaymentMethodId()
    {
        return $this->paymentMethodId;
    }

    public function SetCreated($created)
    {
        $this->created = $created;
    }

    public function Created()
    {
        return $this->created;
    }

    public function SetStatus($status)
    {
        $this->status = $status;
    }

    public function Status()
    {
        return $this->status;
    }
}
