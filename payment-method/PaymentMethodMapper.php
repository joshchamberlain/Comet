<?php

namespace Comet;




require_once(__DIR__ . "/../Mapper.php");
require_once(__DIR__ . "/PaymentMethodFactory.php");




class PaymentMethodMapper extends \Comet\Mapper
{
    public function __construct()
    {
        parent::__construct();

        $this->entityFactory = new \Comet\PaymentMethodFactory();

        $this->table = "comet_payment_methods";

        $this->columns = array
        (
            "id",
            "code",
            "description",
            "type",
            "created",
            "status"
        );

        $this->numColumns = sizeof($this->columns);
    }




    public function CreateTable()
    {
        return $this->RawQuery
        (
            "CREATE TABLE IF NOT EXISTS `" . $this->table . "`
            (
                `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `code` VARCHAR(64) NOT NULL,
                `description` TEXT NOT NULL,
                `type` INT UNSIGNED NOT NULL,
                `created` INT UNSIGNED NOT NULL,
                `status` INT UNSIGNED NOT NULL,
                PRIMARY KEY (`id`)
            ) CHARACTER SET = utf8 COLLATE = utf8_general_ci",
            array()
        );
    }




    public function FindByType($type)
    {
        return $this->FindMultiple
        (
            "WHERE `type`=? ORDER BY `id` ASC",
            array($type)
        );
    }

    public function FindByCreated($minCreated, $maxCreated)
    {
        return $this->FindMultiple
        (
            "WHERE `created`>=? AND `created`<=? ORDER BY `created` ASC",
            array($minCreated, $maxCreated)
        );
    }

    public function FindByStatus($minStatus, $maxStatus)
    {
        return $this->FindMultiple
        (
            "WHERE `status`>=? AND `status`<=? ORDER BY `id` ASC",
            array($minStatus, $maxStatus)
        );
    }
}
