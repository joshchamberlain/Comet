<?php

namespace Comet;




require_once(__DIR__ . "/../PaymentMethod.php");




class StripePaymentMethod extends \Comet\PaymentMethod
{
    protected $paymentMethodId;
    protected $source;
    protected $customer;
    protected $cardHolder;
    protected $cardLastFour;
    protected $cardMonth;
    protected $cardYear;
    protected $cardBrand;




    public function __construct()
    {
        parent::__construct();

        $this->paymentMethodId = 0;
        $this->source = "";
        $this->customer = "";
        $this->cardHolder = "";
        $this->cardLastFour = "";
        $this->cardMonth = "";
        $this->cardYear = "";
        $this->cardBrand = "";
    }




    public function SetPaymentMethodId($paymentMethodId)
    {
        $this->paymentMethodId = $paymentMethodId;
    }

    public function PaymentMethodId()
    {
        return $this->paymentMethodId;
    }

    public function SetSource($source)
    {
        $this->source = $source;
    }

    public function Source()
    {
        return $this->source;
    }

    public function SetCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function Customer()
    {
        return $this->customer;
    }

    public function SetCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    public function CardHolder()
    {
        return $this->cardHolder;
    }

    public function SetCardLastFour($cardLastFour)
    {
        $this->cardLastFour = $cardLastFour;
    }

    public function CardLastFour()
    {
        return $this->cardLastFour;
    }

    public function SetCardMonth($cardMonth)
    {
        $this->cardMonth = $cardMonth;
    }

    public function CardMonth()
    {
        return $this->cardMonth;
    }

    public function SetCardYear($cardYear)
    {
        $this->cardYear = $cardYear;
    }

    public function CardYear()
    {
        return $this->cardYear;
    }

    public function SetCardBrand($cardBrand)
    {
        $this->cardBrand = $cardBrand;
    }

    public function CardBrand()
    {
        return $this->cardBrand;
    }
}
