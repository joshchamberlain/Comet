<?php

namespace Comet;




require_once(__DIR__ . "/../../EntityService.php");
require_once(__DIR__ . "/StripePaymentMethodMapper.php");
require_once(__DIR__ . "/../../../Pluto/storage/StaticDatabaseFactory.php");
require_once(__DIR__ . "/../PaymentMethodEntityServiceFactory.php");
require_once(__DIR__ . "/../../api/stripe/StripeApi.php");




class StripePaymentMethodEntityService extends \Comet\EntityService
{
    protected $db;
    protected $paymentMethodEntityService;
    protected $stripeApi;




    const ERROR_PAYMENT_METHOD_ID = 400;
    const ERROR_SOURCE = 401;
    const ERROR_CUSTOMER = 402;
    const ERROR_CARD_HOLDER = 403;
    const ERROR_CARD_LAST_FOUR = 404;
    const ERROR_CARD_MONTH = 405;
    const ERROR_CARD_YEAR = 406;
    const ERROR_CARD_BRAND = 407;




    public function __construct(\Comet\StripePaymentMethodMapper $stripePaymentMethodMapper)
    {
        parent::__construct($stripePaymentMethodMapper);

        $dbf = new \Pluto\StaticDatabaseFactory();
        $this->db = $dbf->Create();

        $paymentMethodEntityServiceFactory = new \Comet\PaymentMethodEntityServiceFactory();
        $this->paymentMethodEntityService = $paymentMethodEntityServiceFactory->Create();

        $this->stripeApi = new \Comet\StripeApi();

        $this->messageCodeTexts[self::ERROR_PAYMENT_METHOD_ID] = "The payment method ID provided was invalid.";
        $this->messageCodeTexts[self::ERROR_SOURCE] = "The source ID provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CUSTOMER] = "The customer ID provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_HOLDER] = "The cardholder provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_LAST_FOUR] = "The last four digits of the credit card provided were invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_MONTH] = "The card month provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_YEAR] = "The card year provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_BRAND] = "The card brand provided was invalid.";
    }




    public function Create(\Comet\StripePaymentMethod $s)
    {
        $this->result = 1;

        $transactionStarted = $this->db->StartTransaction();

        $stripeCustomer = new \Comet\StripeApiCustomer();
        $stripeCustomer->SetId("");
        $stripeCustomer->SetDescription($s->Description());
        $stripeCustomer->SetDefaultSourceId($s->Source());

        if ($this->stripeApi->CreateCustomer($stripeCustomer))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->Append($this->stripeApi->ProcessedMessageContainer());
            return $this->result;
        }
        
        $stripeCard = $this->stripeApi->RetrieveCard($stripeCustomer->Id(), $stripeCustomer->DefaultSourceId());

        if ($stripeCard === NULL)
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->Append($this->stripeApi->ProcessedMessageContainer());
            return $this->result;
        }

        $s->SetCustomer($stripeCustomer->Id());
        $s->SetSource($stripeCustomer->DefaultSourceId());
        $s->SetCardHolder($stripeCard->Name());
        $s->SetCardLastFour($stripeCard->LastFour());
        $s->SetCardMonth($stripeCard->ExpMonth());
        $s->SetCardYear($stripeCard->ExpYear());
        $s->SetCardBrand($stripeCard->Brand());

        if ($this->paymentMethodEntityService->Create($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->Append($this->paymentMethodEntityService->ProcessedMessageContainer());
            return $this->result;
        }

        if ($this->Validate($s))
        {
            $this->db->Rollback($transactionStarted);
            return $this->result;
        }

        if ($this->mapper && $this->mapper->Create($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $this->result = 0;
        $this->db->Commit($transactionStarted);

        return $this->result;
    }

    public function Update(\Comet\StripePaymentMethod $s)
    {
        $this->result = 1;

        $transactionStarted = $this->db->StartTransaction();

        if ($this->paymentMethodEntityService->Update($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->Append($this->paymentMethodEntityService->ProcessedMessageContainer());
            return $this->result;
        }

        if ($this->Validate($s))
        {
            $this->db->Rollback($transactionStarted);
            return $this->result;
        }

        if ($this->mapper && $this->mapper->Update($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $this->result = 0;
        $this->db->Commit($transactionStarted);

        return $this->result;
    }

    public function Delete(\Comet\StripePaymentMethod $s)
    {
        $this->result = 1;

        $transactionStarted = $this->db->StartTransaction();

        if ($this->paymentMethodEntityService->Delete($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->Append($this->paymentMethodEntityService->ProcessedMessageContainer());
            return $this->result;
        }

        if ($this->mapper && $this->mapper->Delete($s))
        {
            $this->db->Rollback($transactionStarted);
            $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER);
            return $this->result;
        }

        $this->result = 0;
        $this->db->Commit($transactionStarted);

        return $this->result;
    }




    public function Validate(\Comet\StripePaymentMethod $s = NULL)
    {
        if ($s === NULL)
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT);
            return 1;
        }

        if ($this->validator->ValidateInt($s->Id(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_ID);
        }

        if ($this->validator->ValidateString($s->Code(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CODE);
        }

        if ($this->validator->ValidateInt($s->PaymentMethodId(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_PAYMENT_METHOD_ID);
        }

        if ($this->validator->ValidateString($s->Source(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_SOURCE);
        }

        if ($this->validator->ValidateString($s->Customer(), 0))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CUSTOMER);
        }

        if ($this->validator->ValidateString($s->CardHolder(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CARD_HOLDER);
        }

        if ($this->validator->ValidateString($s->CardLastFour(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CARD_LAST_FOUR);
        }

        if ($this->validator->ValidateString($s->CardMonth(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CARD_MONTH);
        }

        if ($this->validator->ValidateString($s->CardYear(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CARD_YEAR);
        }

        if ($this->validator->ValidateString($s->CardBrand(), 1))
        {
            $this->messageContainer->AddErrorMessage(self::ERROR_CARD_BRAND);
        }

        if ($this->messageContainer->NumErrorMessages())
        {
            return 1;
        }

        return 0;
    }




    protected function PopulateEntity(&$o)
    {
        if ($o)
        {
            if ($this->paymentMethodEntityService)
            {
                $p = $this->paymentMethodEntityService->FindById($o->PaymentMethodId());
                $this->paymentMethodEntityService->CopyPaymentMethod($p, $o);
            }
        }

        return $o;
    }
}
