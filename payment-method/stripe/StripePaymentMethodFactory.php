<?php

namespace Comet;




require_once(__DIR__ . "/StripePaymentMethod.php");




class StripePaymentMethodFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Comet\StripePaymentMethod();
    }
}
