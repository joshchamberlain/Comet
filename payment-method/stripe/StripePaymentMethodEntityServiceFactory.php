<?php

namespace Comet;




require_once(__DIR__ . "/StripePaymentMethodEntityService.php");




class StripePaymentMethodEntityServiceFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Comet\StripePaymentMethodEntityService(new \Comet\StripePaymentMethodMapper());
    }
}
