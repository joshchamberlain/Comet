<?php

namespace Comet;




require_once(__DIR__ . "/../PaymentMethodMapper.php");
require_once(__DIR__ . "/StripePaymentMethodFactory.php");




class StripePaymentMethodMapper extends \Comet\PaymentMethodMapper
{
    public function __construct()
    {
        parent::__construct();

        $this->entityFactory = new \Comet\StripePaymentMethodFactory();

        $this->table = "comet_stripe_payment_methods";
        
        $this->columns = array
        (
            "id",
            "code",
            "paymentMethodId",
            "source",
            "customer",
            "cardHolder",
            "cardLastFour",
            "cardMonth",
            "cardYear",
            "cardBrand"
        );

        $this->numColumns = sizeof($this->columns);
    }




    public function CreateTable()
    {
        return $this->RawQuery
        (
            "CREATE TABLE IF NOT EXISTS `" . $this->table . "`
            (
                `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `code` VARCHAR(64) NOT NULL,
                `paymentMethodId` BIGINT UNSIGNED NOT NULL,
                `source` VARCHAR(64) NOT NULL,
                `customer` VARCHAR(64) NOT NULL,
                `cardHolder` VARCHAR(64) NOT NULL,
                `cardLastFour` VARCHAR(4) NOT NULL,
                `cardMonth` VARCHAR(2) NOT NULL,
                `cardYear` VARCHAR(4) NOT NULL,
                `cardBrand` VARCHAR(16) NOT NULL,
                PRIMARY KEY (`id`)
            ) CHARACTER SET = utf8 COLLATE = utf8_general_ci",
            array()
        );
    }




    public function FindByPaymentMethodId($paymentMethodId)
    {
        return $this->FindSingle
        (
            "WHERE `paymentMethodId`=? LIMIT 1",
            array($paymentMethodId)
        );
    }

    public function FindBySource($source)
    {
        return $this->FindSingle
        (
            "WHERE `source`=? LIMIT 1",
            array($source)
        );
    }

    public function FindByCustomer($customer)
    {
        return $this->FindSingle
        (
            "WHERE `customer`=? LIMIT 1",
            array($customer)
        );
    }

    public function FindByCardHolder($cardHolder)
    {
        return $this->FindMultiple
        (
            "WHERE `cardHolder`=? ORDER BY `id` ASC",
            array($cardHolder)
        );
    }

    public function FindByCardLastFour($cardLastFour)
    {
        return $this->FindMultiple
        (
            "WHERE `cardLastFour`=? ORDER BY `id` ASC",
            array($cardLastFour)
        );
    }

    public function FindByCardMonth($cardMonth)
    {
        return $this->FindMultiple
        (
            "WHERE `cardMonth`=? ORDER BY `id` ASC",
            array($cardMonth)
        );
    }

    public function FindByCardYear($cardYear)
    {
        return $this->FindMultiple
        (
            "WHERE `cardYear`=? ORDER BY `id` ASC",
            array($cardYear)
        );
    }
}
