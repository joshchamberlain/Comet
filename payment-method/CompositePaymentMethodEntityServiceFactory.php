<?php

namespace Comet;




require_once(__DIR__ . "/CompositePaymentMethodEntityService.php");




class CompositePaymentMethodEntityServiceFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Comet\CompositePaymentMethodEntityService();
    }
}
