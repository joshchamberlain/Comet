<?php

namespace Comet;




require_once(__DIR__ . "/PaymentMethod.php");




class PaymentMethodFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Comet\PaymentMethod();
    }
}
