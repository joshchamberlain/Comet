<?php

namespace Comet;




require_once(__DIR__ . "/../Service.php");
require_once(__DIR__ . "/PaymentMethodEntityServiceFactory.php");
require_once(__DIR__ . "/stripe/StripePaymentMethodEntityServiceFactory.php");




class CompositePaymentMethodEntityService extends \Comet\Service
{
    protected $paymentMethodEntityService;
    protected $stripePaymentMethodEntityService;




    public function __construct()
    {
        parent::__construct();

        $paymentMethodEntityServiceFactory = new \Comet\PaymentMethodEntityServiceFactory();
        $this->paymentMethodEntityService = $paymentMethodEntityServiceFactory->Create();

        $stripePaymentMethodEntityServiceFactory = new \Comet\StripePaymentMethodEntityServiceFactory();
        $this->stripePaymentMethodEntityService = $stripePaymentMethodEntityServiceFactory->Create();
    }




    public function Create(\Comet\PaymentMethod $p)
    {
        $this->result = 0;

        $s = $this->RetrievePaymentMethodService($p);

        if ($s && $s->Create($p))
        {
            $this->result = 1;
            $this->messageContainer->Append($s->ProcessedMessageContainer());
        }

        return $this->result;
    }

    public function Update(\Comet\PaymentMethod $p)
    {
        $this->result = 0;

        $s = $this->RetrievePaymentMethodService($p);

        if ($s && $s->Update($p))
        {
            $this->result = 1;
            $this->messageContainer->Append($s->ProcessedMessageContainer());
        }

        return $this->result;
    }

    public function Delete(\Comet\PaymentMethod $p)
    {
        $this->result = 0;

        $s = $this->RetrievePaymentMethodService($p);
        
        if ($s && $s->Delete($p))
        {
            $this->result = 1;
            $this->messageContainer->Append($s->ProcessedMessageContainer());
        }

        return $this->result;
    }




    public function FindById($id)
    {
        $p = $this->paymentMethodEntityService->FindById($id);
        $o = $this->FindSpecificPaymentMethod($p);
        return $o;
    }

    public function FindByCode($code)
    {
        $p = $this->paymentMethodEntityService->FindByCode($code);
        $o = $this->FindSpecificPaymentMethod($p);
        return $o;
    }

    public function FindByType($type)
    {
        $p = $this->paymentMethodEntityService->FindByType($type);
        $o = $this->FindSpecificPaymentMethods($p);
        return $o;
    }

    public function FindByCreated($minCreated, $maxCreated)
    {
        $p = $this->paymentMethodEntityService->FindByCreated($minCreated, $maxCreated);
        $o = $this->FindSpecificPaymentMethods($p);
        return $o;
    }

    public function FindByStatus($minStatus, $maxStatus)
    {
        $p = $this->paymentMethodEntityService->FindByStatus($minStatus, $maxStatus);
        $o = $this->FindSpecificPaymentMethods($p);
        return $o;
    }

    public function FindAll()
    {
        $p = $this->paymentMethodEntityService->FindAll();
        $o = $this->FindSpecificPaymentMethods($p);
        return $o;
    }




    protected function FindSpecificPaymentMethod(\Comet\PaymentMethod $p = NULL)
    {
        if ($p === NULL)
        {
            return NULL;
        }

        $s = $this->RetrievePaymentMethodService($p);
        $o = $s ? $s->FindByPaymentMethodId($p->Id()) : NULL;
        
        $this->paymentMethodEntityService->CopyPaymentMethod($p, $o);

        return $o;
    }

    protected function FindSpecificPaymentMethods(Array $p = NULL)
    {
        if ($p === NULL)
        {
            return NULL;
        }

        $objects = array();
        $numPaymentMethods = sizeof($p);

        for ($i = 0; $i < $numPaymentMethods; $i++)
        {
            $o = $this->FindSpecificPaymentMethod($p[$i]);

            if ($o)
            {
                $objects[] = $o;
            }
        }

        return $objects;
    }




    protected function RetrievePaymentMethodService($p)
    {
        $s = NULL;

        switch ($p->Type())
        {
            case \Comet\PaymentMethod::TYPE_STRIPE:
            {
                $s = $this->stripePaymentMethodEntityService;
                break;
            }
        }

        return $s;
    }




    protected function PopulateEntity(&$o)
    {
        return $o;
    }
}
