<?php

namespace Comet;




require_once(__DIR__ . "/../PaymentApi.php");
require_once(__DIR__ . "/StripeCustomer.php");




class StripePaymentApi extends \Comet\PaymentApi
{
    protected $secretKey;
    protected $endpoint;




    const ERROR_API_CONNECTION = 200;
    const ERROR_API = 201;
    const ERROR_AUTHENTICATION = 202;
    const ERROR_CARD = 203;
    const ERROR_INVALID_REQUEST = 204;
    const ERROR_RATE_LIMIT = 205;

    const ERROR_CARD_INVALID_NUMBER = 300;
    const ERROR_CARD_INVALID_EXPIRY_MONTH = 301;
    const ERROR_CARD_INVALID_EXPIRY_YEAR = 302;
    const ERROR_CARD_INVALID_CVC = 303;
    const ERROR_CARD_INCORRECT_NUMBER = 304;
    const ERROR_CARD_EXPIRED = 305;
    const ERROR_CARD_INCORRECT_CVC = 306;
    const ERROR_CARD_INCORRECT_ZIP = 307;
    const ERROR_CARD_DECLINED = 308;
    const ERROR_CARD_MISSING = 309;
    const ERROR_CARD_PROCESSING = 310;




    public function __construct($secretKey, $endpoint = "https://api.stripe.com/v1")
    {
        parent::__construct($secretKey);

        $this->secretKey = $secretKey;
        $this->endpoint = $endpoint;

        $this->messageCodeTexts[self::ERROR_API_CONNECTION] = "Failed to connect to Stripe.";
        $this->messageCodeTexts[self::ERROR_API] = "A Stripe error occurred.";
        $this->messageCodeTexts[self::ERROR_AUTHENTICATION] = "Stripe authentication failed.";
        $this->messageCodeTexts[self::ERROR_CARD] = "A card error occurred.";
        $this->messageCodeTexts[self::ERROR_INVALID_REQUEST] = "The request had invalid parameters.";
        $this->messageCodeTexts[self::ERROR_RATE_LIMIT] = "The rate limit has been reached.";

        $this->messageCodeTexts[self::ERROR_CARD_INVALID_NUMBER] = "The card number provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_EXPIRY_MONTH] = "The card expiration month was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_EXPIRY_YEAR] = "The card expiration year was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_CVC] = "The card CVC was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_NUMBER] = "The card number was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_EXPIRED] = "The card has expired.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_CVC] = "The card CVC was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_ZIP] = "The card postal code was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_DECLINED] = "The card was declined.";
        $this->messageCodeTexts[self::ERROR_CARD_MISSING] = "The card was missing.";
        $this->messageCodeTexts[self::ERROR_CARD_PROCESSING] = "There was an error while processing the card.";
    }




    public function Charge(\Comet\Payment $payment)
    {
        $this->result = 0;

        $postData = array
        (
            "amount" => $payment->Amount(),
            "currency" => $payment->Currency(),
            "source" => strlen($payment->Source()) ? $payment->Source() : NULL,
            "customer" => strlen($payment->Customer()) ? $payment->Customer() : NULL,
            "description" => $payment->Description()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/charges");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }





    public function CreateCustomer(\Comet\StripeCustomer &$stripeCustomer)
    {
        $this->result = 0;

        $postData = array
        (
            "description" => $stripeCustomer->Description(),
            "source" => $stripeCustomer->Source()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        $json = json_decode(curl_exec($ch), TRUE);

        echo $json;

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        if (array_key_exists("id", $json))
        {
            $stripeCustomer->SetId($json["id"]);
            $stripeCustomer->SetSource($json["default_source"]);
        }

        return $this->result;
    }

    public function RetrieveCustomer($customerId)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customerId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("id", $json))
        {
            $c = new \Comet\StripeCustomer();
            $c->SetId($json["id"]);
            $c->SetSource($json["default_source"]);
            $c->SetDescription($json["description"]);
        }

        return $c;
    }

    public function UpdateCustomer(\Comet\StripeCustomer $customer)
    {
        $this->result = 0;

        $postData = array
        (
            "description" => $customer->Description(),
            "source" => $customer->Source()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customer->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json))
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }

    public function DeleteCustomer(\Comet\StripeCustomer $customer)
    {
        $this->result = 0;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customer->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json))
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }




    protected function DetermineError(Array $json)
    {
        $error = $json["error"];
        $type = array_key_exists("type", $error) ? $error["type"] : "";
        $code = array_key_exists("code", $error) ? $error["code"] : "";

        switch ($type)
        {
            case "api_connection_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_API_CONNECTION);
                break;
            }

            case "api_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_API);
                break;
            }

            case "authentication_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_AUTHENTICATION);
                break;
            }

            case "card_error":
            {
                $this->DetermineCardError($code);
                break;
            }

            case "invalid_request_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_INVALID_REQUEST);
                break;
            }

            case "rate_limit_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_RATE_LIMIT);
                break;
            }

            default:
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT);
                break;
            }
        }
    }

    protected function DetermineCardError($code)
    {
        switch ($code)
        {
            case "invalid_number":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_NUMBER);
                break;
            }

            case "invalid_expiry_month":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_EXPIRY_MONTH);
                break;
            }

            case "invalid_expiry_year":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_EXPIRY_YEAR);
                break;
            }

            case "invalid_cvc":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_CVC);
                break;
            }

            case "incorrect_number":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_NUMBER);
                break;
            }

            case "expired_card":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_EXPIRED);
            }

            case "incorrect_cvc":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_CVC);
                break;
            }

            case "incorrect_zip":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_ZIP);
                break;
            }

            case "card_declined":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_DECLINED);
                break;
            }

            case "missing":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_MISSING);
                break;
            }

            case "processing_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_PROCESSING);
                break;
            }

            default:
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD);
                break;
            }
        }
    }
}
