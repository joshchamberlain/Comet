<?php

namespace Comet;




class StripeCustomer
{
    protected $id;
    protected $source;
    protected $description;




    public function __construct()
    {
        $this->id = "";
        $this->source = "";
        $this->description = "";
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetSource($source)
    {
        $this->source = $source;
    }

    public function Source()
    {
        return $this->source;
    }

    public function SetDescription($description)
    {
        $this->description = $description;
    }

    public function Description()
    {
        return $this->description;
    }
}
