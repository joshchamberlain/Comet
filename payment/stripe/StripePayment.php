<?php

namespace Comet;




require_once(__DIR__ . "/../Payment.php");




class StripePayment extends \Comet\Payment
{
    protected $source;
    protected $customer;




    public function __construct()
    {
        parent::__construct();

        $this->source = "";
        $this->customer = "";
    }




    public function SetSource($source)
    {
        $this->source = $source;
    }

    public function Source()
    {
        return $this->source;
    }

    public function SetCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function Customer()
    {
        return $this->customer;
    }
}
