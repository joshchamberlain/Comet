<?php

namespace Comet;




require_once(__DIR__ . "/../Mapper.php");
require_once(__DIR__ . "/PaymentFactory.php");




abstract class AbstractPaymentMapper extends \Comet\Mapper
{
    public function __construct()
    {
        parent::__construct();

        $this->entityFactory = new \Comet\PaymentFactory();
    }




    public function CreateTable()
    {
    }
}
