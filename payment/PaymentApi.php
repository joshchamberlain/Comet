<?php

namespace Comet;




require_once(__DIR__ . "/../Api.php");
require_once(__DIR__ . "/Payment.php");




abstract class PaymentApi extends \Comet\Api
{
    public function __construct()
    {
        parent::__construct();

        $this->messageCodeTexts[self::ERROR_DEFAULT] = "An error occurred with the payment gateway. Please try again.";
    }




    abstract public function Charge(\Comet\Payment $payment);
}
