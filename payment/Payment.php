<?php

namespace Comet;




class Payment
{
    protected $amount;
    protected $currency;
    protected $description;
    protected $created;
    protected $status;




    public function __construct()
    {
        $this->amount = 0;
        $this->currency = "usd";
        $this->source = "";
        $this->customer = "";
        $this->description = "";
    }




    public function SetAmount($amount)
    {
        $this->amount = $amount;
    }

    public function Amount()
    {
        return $this->amount;
    }

    public function SetCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function Currency()
    {
        return $this->currency;
    }

    public function SetSource($source)
    {
        $this->source = $source;
    }

    public function Source()
    {
        return $this->source;
    }

    public function SetCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function Customer()
    {
        return $this->customer;
    }

    public function SetDescription($description)
    {
        $this->description = $description;
    }

    public function Description()
    {
        return $this->description;
    }
}
