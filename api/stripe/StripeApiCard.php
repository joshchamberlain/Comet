<?php

namespace Comet;




class StripeApiCard
{
    protected $id;
    protected $customerId;
    protected $brand;
    protected $name;
    protected $lastFour;
    protected $expMonth;
    protected $expYear;
    protected $address1;
    protected $address2;
    protected $city;
    protected $state;
    protected $zip;
    protected $country;




    public function __construct()
    {
        $this->id = "";
        $this->customerId = "";
        $this->brand = "";
        $this->name = "";
        $this->lastFour = "";
        $this->expMonth = "";
        $this->expYear = "";
        $this->address1 = "";
        $this->address2 = "";
        $this->city = "";
        $this->state = "";
        $this->zip = "";
        $this->country = "";
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function CustomerId()
    {
        return $this->customerId;
    }

    public function SetBrand($brand)
    {
        $this->brand = $brand;
    }

    public function Brand()
    {
        return $this->brand;
    }

    public function SetName($name)
    {
        $this->name = $name;
    }

    public function Name()
    {
        return $this->name;
    }

    public function SetLastFour($lastFour)
    {
        $this->lastFour = $lastFour;
    }

    public function LastFour()
    {
        return $this->lastFour;
    }

    public function SetExpMonth($expMonth)
    {
        $this->expMonth = $expMonth;
    }

    public function ExpMonth()
    {
        return $this->expMonth;
    }

    public function SetExpYear($expYear)
    {
        $this->expYear = $expYear;
    }

    public function ExpYear()
    {
        return $this->expYear;
    }

    public function SetAddress1($address1)
    {
        $this->address1 = $address1;
    }

    public function Address1()
    {
        return $this->address1;
    }

    public function SetAddress2($address2)
    {
        $this->address2 = $address2;
    }

    public function Address2()
    {
        return $this->address2;
    }

    public function SetCity($city)
    {
        $this->city = $city;
    }

    public function City()
    {
        return $this->city;
    }

    public function SetState($state)
    {
        $this->state = $state;
    }

    public function State()
    {
        return $this->state;
    }

    public function SetZip($zip)
    {
        $this->zip = $zip;
    }

    public function Zip()
    {
        return $this->zip;
    }

    public function SetCountry($country)
    {
        $this->country = $country;
    }

    public function Country()
    {
        return $this->country;
    }
}
