<?php

namespace Comet;




require_once(__DIR__ . "/../Api.php");
require_once(__DIR__ . "/../../Config.php");
require_once(__DIR__ . "/StripeApiCard.php");
require_once(__DIR__ . "/StripeApiCharge.php");
require_once(__DIR__ . "/StripeApiCustomer.php");
require_once(__DIR__ . "/StripeApiDispute.php");




class StripeApi extends \Comet\Api
{
    protected $secretKey;
    protected $endpoint;




    const ERROR_API_CONNECTION = 300;
    const ERROR_API = 301;
    const ERROR_AUTHENTICATION = 302;
    const ERROR_CARD = 303;
    const ERROR_INVALID_REQUEST = 304;
    const ERROR_RATE_LIMIT = 305;

    const ERROR_CARD_INVALID_NUMBER = 400;
    const ERROR_CARD_INVALID_EXPIRY_MONTH = 401;
    const ERROR_CARD_INVALID_EXPIRY_YEAR = 402;
    const ERROR_CARD_INVALID_CVC = 403;
    const ERROR_CARD_INCORRECT_NUMBER = 404;
    const ERROR_CARD_EXPIRED = 405;
    const ERROR_CARD_INCORRECT_CVC = 406;
    const ERROR_CARD_INCORRECT_ZIP = 407;
    const ERROR_CARD_DECLINED = 408;
    const ERROR_CARD_MISSING = 409;
    const ERROR_CARD_PROCESSING = 410;




    public function __construct($secretKey = NULL, $endpoint = "https://api.stripe.com/v1")
    {
        parent::__construct();

        if ($secretKey === NULL)
        {
            $secretKey = \Comet\Config::Data("stripe_secret_key");
        }

        $this->secretKey = $secretKey;
        $this->endpoint = $endpoint;

        $this->messageCodeTexts[self::ERROR_API_CONNECTION] = "Failed to connect to Stripe.";
        $this->messageCodeTexts[self::ERROR_API] = "A Stripe error occurred.";
        $this->messageCodeTexts[self::ERROR_AUTHENTICATION] = "Stripe authentication failed.";
        $this->messageCodeTexts[self::ERROR_CARD] = "A card error occurred.";
        $this->messageCodeTexts[self::ERROR_INVALID_REQUEST] = "The request had invalid parameters.";
        $this->messageCodeTexts[self::ERROR_RATE_LIMIT] = "The rate limit has been reached.";

        $this->messageCodeTexts[self::ERROR_CARD_INVALID_NUMBER] = "The card number provided was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_EXPIRY_MONTH] = "The card expiration month was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_EXPIRY_YEAR] = "The card expiration year was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INVALID_CVC] = "The card CVC was invalid.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_NUMBER] = "The card number was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_EXPIRED] = "The card has expired.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_CVC] = "The card CVC was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_INCORRECT_ZIP] = "The card ZIP was incorrect.";
        $this->messageCodeTexts[self::ERROR_CARD_DECLINED] = "The card was declined.";
        $this->messageCodeTexts[self::ERROR_CARD_MISSING] = "The card was missing.";
        $this->messageCodeTexts[self::ERROR_CARD_PROCESSING] = "There was an error while processing the card.";
    }




    public function CreateCharge(\Comet\StripeApiCharge &$c)
    {
        $this->result = 0;

        $postData = array
        (
            "amount" => $c->Amount(),
            "currency" => $c->Currency(),
            "source" => strlen($c->SourceId()) ? $c->SourceId() : NULL,
            "customer" => strlen($c->CustomerId()) ? $c->CustomerId() : NULL,
            "description" => $c->Description()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/charges");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        
        $json = json_decode(curl_exec($ch), TRUE);

        echo $json;

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        else
        {
            $this->PopulateCharge($c, $json);
        }

        return $this->result;
    }

    public function RetrieveCharge($id)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/charges/" . $id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("id", $json) === TRUE)
        {
            $c = $this->BuildCharge($json);
        }

        return $c;
    }

    public function UpdateCharge(\Comet\StripeApi &$c)
    {
        $this->result = 0;

        $postData = array
        (
            "description" => $c->Description()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/charges/" . $c->Id());
        curl_setopt($ch, CURL_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CUROPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }

    public function ListCharges($amount)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/charges?limit=" . $amount);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("data", $json) === TRUE)
        {
            $c = $this->BuildCharges($json);
        }

        return $c;
    }




    public function CreateCustomer(\Comet\StripeApiCustomer &$c)
    {
        $this->result = 0;

        $postData = array
        (
            "description" => $c->Description(),
            "source" => $c->DefaultSourceId()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        else
        {
            $this->PopulateCustomer($c, $json);
        }

        return $this->result;
    }

    public function RetrieveCustomer($id)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("id", $json) === TRUE)
        {
            $c = $this->BuildCustomer($json);
        }

        return $c;
    }

    public function UpdateCustomer(\Comet\StripeApiCustomer &$c)
    {
        $this->result = 0;

        $postData = array
        (
            "description" => $c->Description(),
            "default_source" => $c->DefaultSourceId()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $c->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }

    public function DeleteCustomer(\Comet\StripeApiCustomer &$c)
    {
        $this->result = 0;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $c->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }
    }

    public function ListCustomers($amount = 100)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers?limit=" . $amount);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("data", $json) === TRUE)
        {
            $c = $this->BuildCustomers($json);
        }

        return $c;
    }




    public function CreateCard($customerId, $sourceId)
    {
        $this->result = 0;
        
        $c = NULL;

        $postData = array
        (
            "source" => $sourceId
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customerId . "/sources");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        else
        {
            $c = $this->BuildCard($json);
        }

        return $c;
    }

    public function RetrieveCard($customerId, $id)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customerId . "/sources/" . $id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("id", $json) === TRUE)
        {
            $c = $this->BuildCard($json);
        }

        return $c;
    }

    public function UpdateCard(\Comet\StripeApiCard &$c)
    {
        $this->result = 0;

        $postData = array
        (
            "name" => $c->Name(),
            "exp_month" => $c->ExpMonth(),
            "exp_year" => $c->ExpYear(),
            "address_zip" => $c->Zip()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $c->CustomerId() . "/sources/" . $c->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }

    public function DeleteCard(\Comet\StripeApiCard &$c)
    {
        $this->result = 0;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $c->CustomerId() . "/sources/" . $c->Id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");
        
        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("error", $json) === TRUE)
        {
            $this->result = 1;
            $this->DetermineError($json);
        }

        return $this->result;
    }

    public function ListCards($customerId, $amount = 100)
    {
        $c = NULL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/customers/" . $customerId . "/sources?object=card&limit=" . $amount);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

        $json = json_decode(curl_exec($ch), TRUE);

        if (array_key_exists("data", $json) === TRUE)
        {
            $c = $this->BuildCards($json);
        }

        return $c;
    }




    public function RetrieveDispute($id)
    {
    }

    public function UpdateDispute(\Comet\StripeApiDispute &$d)
    {
    }

    public function CloseDispute(\Comet\StripeApiDispute &$d)
    {
    }

    public function ListDisputes($amount)
    {
    }




    public function PopulateCharge(\Comet\StripeApiCharge &$c, Array $json)
    {
        $c->SetId(array_key_exists("id", $json) ? $json["id"] : "");
        $c->SetAmount(array_key_exists("amount", $json) ? $json["amount"] : "");
        $c->SetCurrency(array_key_exists("currency", $json) ? $json["currency"] : "");
        $c->SetCustomerId(array_key_exists("customer", $json) ? $json["customer"] : "");
        $c->SetDescription(array_key_exists("description", $json) ? $json["description"] : "");
        $c->SetSourceId(array_key_exists("source", $json) ? $json["source"]["id"] : "");
    }

    public function PopulateCustomer(\Comet\StripeApiCustomer &$c, Array $json)
    {
        $c->SetId(array_key_exists("id", $json) ? $json["id"] : "");
        $c->SetDescription(array_key_exists("description", $json) ? $json["description"] : "");
        $c->SetDefaultSourceId(array_key_exists("default_source", $json) ? $json["default_source"] : "");
        
        if (array_key_exists("sources", $json))
        {
            $sourceIds = array();
            $sources = $json["sources"]["data"];
            $numSources = sizeof($sources);

            for ($i = 0; $i < $numSources; $i++)
            {
                if (array_key_exists("id", $sources[$i]) === TRUE)
                {
                    $sourceIds[] = $sources[$i]["id"];
                }
            }
        }
    }

    public function PopulateCard(\Comet\StripeApiCard &$c, Array $json)
    {
        $c->SetId(array_key_exists("id", $json) ? $json["id"] : "");
        $c->SetCustomerId(array_key_exists("customer", $json) ? $json["customer"] : "");
        $c->SetBrand(array_key_exists("brand", $json) ? $json["brand"] : "");
        $c->SetName(array_key_exists("name", $json) ? $json["name"] : "");
        $c->SetLastFour(array_key_exists("last4", $json) ? $json["last4"] : "");
        $c->SetExpMonth(array_key_exists("exp_month", $json) ? $json["exp_month"] : "");
        $c->SetExpYear(array_key_exists("exp_year", $json) ? $json["exp_year"] : "");
        $c->SetAddress1(array_key_exists("address_line1", $json) ? $json["address_line1"] : "");
        $c->SetAddress2(array_key_exists("address_line2", $json) ? $json["address_line2"] : "");
        $c->SetCity(array_key_exists("address_city", $json) ? $json["address_city"] : "");
        $c->SetState(array_key_exists("address_state", $json) ? $json["address_state"] : "");
        $c->SetZip(array_key_exists("address_zip", $json) ? $json["address_zip"] : "");
        $c->SetCountry(array_key_exists("address_country", $json) ? $json["address_country"] : "");
    }




    public function BuildCharge(Array $json)
    {
        $c = new \Comet\StripeApiCharge();
        $this->PopulateCharge($c, $json);
        return $c;
    }

    public function BuildCharges(Array $json)
    {
        $c = NULL;

        if (array_key_exists("data", $json) === FALSE)
        {
            return $c;
        }

        $c = array();
        $charges = $json["data"];
        $numCharges = sizeof($charges);

        for ($i = 0; $i < $numCharges; $i++)
        {
            $charge = $this->BuildCharge($charges[$i]);

            if ($charge)
            {
                $c[] = $charge;
            }
        }

        return $c;
    }

    public function BuildCustomer(Array $json)
    {
        $c = new \Comet\StripeApiCustomer();
        $this->PopulateCustomer($c, $json);
        return $c;
    }

    public function BuildCustomers(Array $json)
    {
        $c = NULL;

        if (array_key_exists("data", $json) === FALSE)
        {
            return $c;
        }

        $c = array();
        $customers = $json["data"];
        $numCustomers = sizeof($customers);

        for ($i = 0; $i < $numCustomers; $i++)
        {
            $customer = $this->BuildCustomer($customers[$i]);

            if ($customer)
            {
                $c[] = $customer;
            }
        }

        return $c;
    }

    public function BuildCard(Array $json)
    {
        $c = new \Comet\StripeApiCard();
        $this->PopulateCard($c, $json);
        return $c;
    }

    public function BuildCards(Array $json)
    {
        $c = NULL;

        if (array_key_exists("data", $json) === FALSE)
        {
            return $c;
        }

        $c = array();
        $cards = $json["data"];
        $numCards = sizeof($cards);

        for ($i = 0; $i < $numCards; $i++)
        {
            $card = $this->BuildCard($cards[$i]);
            
            if ($card)
            {
                $c[] = $card;
            }
        }

        return $c;
    }




    protected function DetermineError(Array $json)
    {
        $error = $json["error"];
        $type = array_key_exists("type", $error) ? $error["type"] : "";
        $code = array_key_exists("code", $error) ? $error["code"] : "";

        switch ($type)
        {
            case "api_connection_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_API_CONNECTION);
                break;
            }

            case "api_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_API);
                break;
            }

            case "authentication_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_AUTHENTICATION);
                break;
            }

            case "card_error":
            {
                $this->DetermineCardError($code);
                break;
            }

            case "invalid_request_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_INVALID_REQUEST);
                break;
            }

            case "rate_limit_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_RATE_LIMIT);
                break;
            }

            default:
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT);
                break;
            }
        }
    }

    protected function DetermineCardError($code)
    {
        switch ($code)
        {
            case "invalid_number":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_NUMBER);
                break;
            }

            case "invalid_expiry_month":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_EXPIRY_MONTH);
                break;
            }

            case "invalid_expiry_year":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_EXPIRY_YEAR);
                break;
            }

            case "invalid_cvc":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INVALID_CVC);
                break;
            }

            case "incorrect_number":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_NUMBER);
                break;
            }

            case "expired_card":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_EXPIRED_CARD);
                break;
            }

            case "incorrect_cvc":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_CVC);
                break;
            }

            case "incorrect_zip":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_INCORRECT_ZIP);
                break;
            }

            case "card_declined":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_DECLINED);
                break;
            }

            case "missing":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_MISSING);
                break;
            }

            case "processing_error":
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD_PROCESSING);
                break;
            }

            default:
            {
                $this->messageContainer->AddErrorMessage(self::ERROR_CARD);
                break;
            }
        }
    }
}
