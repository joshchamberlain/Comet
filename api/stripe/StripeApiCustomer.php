<?php

namespace Comet;




class StripeApiCustomer
{
    protected $id;
    protected $description;
    protected $defaultSourceId;
    protected $sourceIds;




    public function __construct()
    {
        $this->id = "";
        $this->description = "";
        $this->defaultSourceId = "";
        $this->sourceIds = array();
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetDescription($description)
    {
        $this->description = $description;
    }

    public function Description()
    {
        return $this->description;
    }

    public function SetDefaultSourceId($defaultSourceId)
    {
        $this->defaultSourceId = $defaultSourceId;
    }

    public function DefaultSourceId()
    {
        return $this->defaultSourceId;
    }

    public function SetSourceIds(Array $sourceIds)
    {
        $this->sourceIds = $sourceIds;
    }

    public function SourceIds()
    {
        return $this->sourceIds;
    }
}
