<?php

namespace Comet;




class StripeApiCharge
{
    protected $id;
    protected $amount;
    protected $currency;
    protected $customerId;
    protected $description;
    protected $sourceId;




    public function __construct()
    {
        $this->id = "";
        $this->amount = 0;
        $this->currency = "";
        $this->customerId = "";
        $this->description = "";
        $this->sourceId = "";
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetAmount($amount)
    {
        $this->amount = $amount;
    }

    public function Amount()
    {
        return $this->amount;
    }

    public function SetCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function Currency()
    {
        return $this->currency;
    }

    public function SetCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function CustomerId()
    {
        return $this->customerId;
    }

    public function SetDescription($description)
    {
        $this->description = $description;
    }

    public function Description()
    {
        return $this->description;
    }

    public function SetSourceId($sourceId)
    {
        $this->sourceId = $sourceId;
    }

    public function SourceId()
    {
        return $this->sourceId;
    }
}
