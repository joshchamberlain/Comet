<?php

namespace Comet;




class StripeApiDispute
{
    protected $id;
    protected $amount;
    protected $chargeId;
    protected $evidence;
    protected $evidenceDetails;
    protected $reason;
    protected $status;




    public function __construct()
    {
        $this->id = "";
        $this->amount = 0;
        $this->chargeId = "";
        $this->evidence = array();
        $this->evidenceDetails = array();
        $this->reason = "";
        $this->status = "";
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetAmount($amount)
    {
        $this->amount = $amount;
    }

    public function Amount()
    {
        return $this->amount;
    }

    public function SetChargeId($chargeId)
    {
        $this->chargeId = $chargeId;
    }

    public function ChargeId()
    {
        return $this->chargeId;
    }

    public function SetEvidence(Array $evidence)
    {
        $this->evidence = $evidence;
    }

    public function Evidence()
    {
        return $this->evidence;
    }

    public function SetEvidenceDetails(Array $evidenceDetails)
    {
        $this->evidenceDetails = $evidenceDetails;
    }

    public function EvidenceDetails()
    {
        return $this->evidenceDetails;
    }

    public function SetReason($reason)
    {
        $this->reason = $reason;
    }

    public function Reason()
    {
        return $this->reason;
    }

    public function SetStatus($status)
    {
        $this->status = $status;
    }

    public function Status()
    {
        return $this->status;
    }
}
