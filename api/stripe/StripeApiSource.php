<?php

namespace Comet;




class StripeApiSource
{
    protected $id;
    protected $type;
    protected $data;




    public function __construct()
    {
        $this->id = "";
        $this->type = "";
        $this->data = array();
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetType($type)
    {
        $this->type = $type;
    }

    public function Type()
    {
        return $this->type;
    }

    public function SetData(Array $data)
    {
        $this->data = $data;
    }

    public function Data()
    {
        return $this->data;
    }
}
