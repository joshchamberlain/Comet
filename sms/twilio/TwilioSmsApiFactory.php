<?php

namespace Comet;




require_once(__DIR__ . "/../SmsApiFactory.php");
require_once(__DIR__ . "/TwilioSmsApi.php");




class TwilioSmsApiFactory extends \Comet\SmsApiFactory
{
    public function __construct()
    {
        parent::__construct();
    }




    public function Create($sid, $authToken, $endpoint = "https://api.twilio.com/2010-04-01")
    {
        return new \Comet\TwilioSmsApi($sid, $authToken, $endpoint);
    }
}
