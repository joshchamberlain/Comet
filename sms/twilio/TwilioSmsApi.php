<?php

namespace Comet;




require_once(__DIR__ . "/../SmsApi.php");




class TwilioSmsApi extends \Comet\SmsApi
{
    protected $sid;
    protected $authToken;
    protected $endpoint;




    public function __construct($sid, $authToken, $endpoint = "https://api.twilio.com/2010-04-01")
    {
        parent::__construct();

        $this->sid = $sid;
        $this->authToken = $authToken;
        $this->endpoint = $endpoint;
    }




    public function Send(\Comet\Sms $sms)
    {
        $this->result = 1;

        $postData = array
        (
            "To" => $sms->Recipient(),
            "From" => $sms->Sender(),
            "Body" => $sms->Message()
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . "/Accounts/" . $this->sid . "/Messages.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $this->sid . ":" . $this->authToken);
        $json = json_decode(curl_exec($ch), TRUE);

        print_r($json);

        $this->result = $this->DetermineError($json);

        return $this->result;
    }




    protected function DetermineError(Array $json)
    {
        if (array_key_exists("code", $json) === FALSE)
        {
            return 0;
        }

        $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT);

        return 1;

        /*
        switch ($json["code"])
        {
            case "
        }
        */
    }
}
