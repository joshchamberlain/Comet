<?php

namespace Comet;




class Sms
{
    protected $sender;
    protected $recipient;
    protected $message;




    public function __construct()
    {
        $this->sender = "";
        $this->recipient = "";
        $this->message = "";
    }




    public function SetSender($sender)
    {
        $this->sender = $sender;
    }

    public function Sender()
    {
        return $this->sender;
    }

    public function SetRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    public function Recipient()
    {
        return $this->recipient;
    }
    
    public function SetMessage($message)
    {
        $this->message = $message;
    }

    public function Message()
    {
        return $this->message;
    }
}
