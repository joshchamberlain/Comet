<?php

namespace Comet;




require_once(__DIR__ . "/../Api.php");
require_once(__DIR__ . "/Sms.php");




abstract class SmsApi extends \Comet\Api
{
    public function __construct()
    {
        parent::__construct();

        $this->messageCodeTexts[self::ERROR_DEFAULT] = "An error occurred with the SMS gateway. Please try again.";
    }




    abstract public function Send(\Comet\Sms $sms);
}
